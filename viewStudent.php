<?php
$db = new PDO('mysql:host=localhost;dbname=Student;charset=utf8mb4', 'root', '');
$query="SELECT * FROM `std-reg`";
$stmt=$db->query($query);
$data=$stmt->fetchAll(PDO::FETCH_ASSOC);
?>

<html>
<head>
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <link rel="stylesheet" href="css/bootstrap-theme.css">
</head>

<body>
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <nav class="navbar navbar-default">
                <div class="container-fluid">
                    <div class="navbar-header">
                        <a class="navbar-brand" href="#">Student Info</a>
                    </div>
                    <ul class="nav navbar-nav">

                        <li><a href="reg.php">Registration</a></li>
                        <li><a href="viewStudent.php">All Student</a></li>
                        <li><a href="course.php">Course</a></li>

                        <li><a href="assign.php">Assign</a></li>
                    </ul>
                </div>
            </nav>


        </div>
    </div>
</div>


<div class="container">
    <table class="table table-bordered table-responsive">
        <thead>
            <tr>
                <th>stdID</th>
                <th>Name</th>
                <th>phone Number</th>
                <th>Picture</th>
                <th>Action</th>

            </tr>
        </thead>

        <tbody>
        <?php
            $serial=0;
            foreach ($data as $vstd){
                $serial++;

        ?>
            <tr>
                <td><?php echo $serial; ?></td>
                <td> <?php echo $vstd['name'] ?> </td>
                <td> <?php echo $vstd['mobile'] ?> </td>
                <td align="center"><img src="images/<?php echo $vstd['picture']?>" alt="Smiley face" height="50" width="50"></td>
                <td>
                    <a href="stdDetails.php?id=<?php echo $vstd['id'];?>">View</a>
                    <a href="stdEdit.php?id=<?php echo $vstd['id'];?>">Edit</a>
                    <a href="stdDelete.php?id=<?php echo $vstd['id'];?>">Delete</a>
                </td>
            </tr>

        <?php } ?>
        </tbody>
    </table>
</div>
</body>

</html>