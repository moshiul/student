<?php
$db = new PDO('mysql:host=localhost;dbname=Student;charset=utf8mb4', 'root', '');
$query="SELECT * FROM `student_course_reg`";
$stmt=$db->query($query);
$result=$stmt->fetchAll(PDO::FETCH_ASSOC);
?>


<html>
<head>
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/bootstrap-theme.css">
</head>

<body>
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <nav class="navbar navbar-default">
                <div class="container-fluid">
                    <div class="navbar-header">
                        <a class="navbar-brand" href="#">Student Info</a>
                    </div>
                    <ul class="nav navbar-nav">

                        <li><a href="reg.php">Registration</a></li>
                        <li><a href="viewStudent.php">All Student</a></li>
                        <li><a href="course.php">Course</a></li>

                        <li><a href="assign.php">Assign</a></li>
                    </ul>
                </div>
            </nav>


        </div>
    </div>
</div>

<div class="container">
    <table class="table table-bordered table-responsive">
        <thead>
            <tr>
                <th>No.</th>
                <th>Course Title</th>
                <th>Student ID</th>
            </tr>
        </thead>

        <tbody>

        <?php
        $no=0;
        foreach ($result as $vac) {
            $no++;
            ?>

            <tr>
                <td> <?php echo $no ?> </td>
                <td> <?php echo $vac['course_title'] ?> </td>
                <td> <?php echo $vac['student_id'] ?> </td>
            </tr>

            <?php
        }
        ?>
        </tbody>
    </table>
</div>

</body>
</html>