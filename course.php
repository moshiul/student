<?php
?>



<html>
<head>
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/bootstrap-theme.css">
</head>



<body>

<div class="container">
    <div class="row">
        <div class="col-md-12">
            <nav class="navbar navbar-default">
                <div class="container-fluid">
                    <div class="navbar-header">
                        <a class="navbar-brand" href="#">Student Info</a>
                    </div>
                    <ul class="nav navbar-nav">

                        <li><a href="reg.php">Registration</a></li>
                        <li><a href="viewStudent.php">All Student</a></li>
                        <li><a href="course.php">Course</a></li>

                        <li><a href="assign.php">Assign</a></li>
                    </ul>
                </div>
            </nav>


        </div>
    </div>
</div>

<div class="container">
    <div class="row">
        <div class="col-md-6">
            <form action="insertCourse.php" method="post" enctype="multipart/form-data">

                <div class="box">

                    <div class="form-group">
                        <label> Course ID </label>
                        <input type="text" name="cid" class="form-control">
                    </div>

                    <div class="form-group">
                        <label> Course Title </label>
                        <input type="text" name="ctitle" class="form-control">
                    </div>

                    <div class="form-group">
                        <label> Course Credit </label>
                        <input type="text" name="ccredit" class="form-control">
                    </div>

                    <div class="form-group">
                        <label> Course Hour </label>
                        <input type="text" name="chour" class="form-control">
                    </div>
                </div>


                <div class="form-group">
                    <button type="submit" class="btn btn-primary">Submit</button>
                    <button type="reset" class="btn btn-primary">Reset</button>
                </div>

            </form>
        </div>
    </div>

</div>


</body>
</html>
