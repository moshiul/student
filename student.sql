-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Sep 17, 2017 at 08:25 PM
-- Server version: 10.1.19-MariaDB
-- PHP Version: 5.6.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `student`
--

-- --------------------------------------------------------

--
-- Table structure for table `course`
--

CREATE TABLE `course` (
  `course_id` int(11) NOT NULL,
  `course_title` varchar(255) NOT NULL,
  `course_credit` int(255) NOT NULL,
  `course_hour` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `course`
--

INSERT INTO `course` (`course_id`, `course_title`, `course_credit`, `course_hour`) VALUES
(101, 'computer fundamenatl', 4, 4),
(102, 'English', 3, 4),
(103, 'physics', 4, 4),
(106, 'css', 4, 4),
(201, 'Math', 4, 4),
(456, 'robot', 4, 4);

-- --------------------------------------------------------

--
-- Table structure for table `std-reg`
--

CREATE TABLE `std-reg` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL,
  `mobile` varchar(255) NOT NULL,
  `gender` varchar(255) NOT NULL,
  `picture` varchar(255) NOT NULL,
  `dob` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `std-reg`
--

INSERT INTO `std-reg` (`id`, `name`, `email`, `password`, `address`, `mobile`, `gender`, `picture`, `dob`) VALUES
(24, 'Dinner', 'moshiulislam42@gmail.com', '123qwer', 'Dhaka', '01749912520', 'male', '20232317_870383609801742_7380113145694294878_o.jpg', '14 - 3 - 1993'),
(26, 'shawon khondokar', 'shawon@gmail.com', 'shawon', 'Borishal', '01674580972', 'male', '20347967_500388556967411_1343998162_o.jpg', '17 - 7 - 1997');

-- --------------------------------------------------------

--
-- Table structure for table `student_course_reg`
--

CREATE TABLE `student_course_reg` (
  `id` int(11) NOT NULL,
  `student_id` int(11) NOT NULL,
  `course_title` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `student_course_reg`
--

INSERT INTO `student_course_reg` (`id`, `student_id`, `course_title`) VALUES
(13, 21, 'php,robot'),
(14, 24, 'php,css'),
(15, 24, 'css'),
(16, 26, 'English,Math'),
(17, 26, 'css,English');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `course`
--
ALTER TABLE `course`
  ADD PRIMARY KEY (`course_id`);

--
-- Indexes for table `std-reg`
--
ALTER TABLE `std-reg`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `student_course_reg`
--
ALTER TABLE `student_course_reg`
  ADD PRIMARY KEY (`id`),
  ADD KEY `student_id` (`student_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `std-reg`
--
ALTER TABLE `std-reg`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;
--
-- AUTO_INCREMENT for table `student_course_reg`
--
ALTER TABLE `student_course_reg`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
