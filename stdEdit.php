<?php

$db = new PDO('mysql:host=localhost;dbname=Student;charset=utf8mb4', 'root', '');
$query="SELECT * FROM `std-reg`where id=".$_GET['id'];
$stmt=$db->query($query);
$data=$stmt->fetch(PDO::FETCH_ASSOC);

?>




<html>
<head>
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/bootstrap-theme.css">

</head>
<body>


<div class="container">
    <div class="row">
        <div class="col-md-12">
            <nav class="navbar navbar-default">
                <div class="container-fluid">
                    <div class="navbar-header">
                        <a class="navbar-brand" href="#">Student Info</a>
                    </div>
                    <ul class="nav navbar-nav">

                        <li><a href="reg.php">Registration</a></li>
                        <li><a href="viewStudent.php">All Student</a></li>
                        <li><a href="course.php">Course</a></li>

                        <li><a href="assign.php">Assign</a></li>
                    </ul>
                </div>
            </nav>


        </div>
    </div>
</div>

<div class="container">
    <div class="row">
        <div class="col-md-6">
            <form action="updateStudent.php" method="post" enctype="multipart/form-data">

                <div class="box">

                    <div class="form-group">
                        <label> Enter Your Name </label>
                        <input type="text" value="<?php echo $data['name']?>" name="name" class="form-control">
                        <input type="hidden" value="<?php echo $data['id']?>" name="id" class="form-control">
                    </div>

                    <div class="form-group">
                        <label> Enter Your Email </label>
                        <input type="text" value="<?php echo $data['email']?>" name="email" class="form-control">
                    </div>

                    <div class="form-group">
                        <label> Enter Your Password </label>
                        <input type="text" value="<?php echo $data['password']?>" name="password" class="form-control">
                    </div>

                    <div class="form-group">
                        <label> Enter Your Address </label>
                        <textarea name="address" name="address" class="form-control"><?php echo $data['address']?></textarea>
                    </div>

                    <div class="form-group">
                        <label> Enter Your Mobile </label>
                        <input type="text" value="<?php echo $data['mobile']?>" name="mobile" class="form-control">
                    </div>
                </div>

                <div class="form-group">
                    <label>Select Your Gender</label>

                    <input  id="male" type="radio" <?php echo ($data['gender']=='male')?'checked':'' ?> name="gender" value="male">
                    <label for="male">Male</label>

                    <input type="radio" <?php echo ($data['gender']=='female')?'checked':'' ?> id="female" name="gender" value="female">
                    <label for="female">Female</label>
                </div>





                <?php

                $date = explode('-', $data['dob']);

                ?>

                <div class="form-group">
                    <label>Select your DOB:</label>
                    <select name="day" class="btn btn-default">
                        <option value="">Day</option>
                        <?php
                        for($i=1; $i<=31; $i++){
                            if($i==$date[0]){
                                $key = 'selected';
                            }else{
                                $key = '';
                            }
                            echo "<option $key value='$i'>$i</option>";
                        }
                        ?>


                    </select>
                    <select name="month" class="btn btn-default">
                        <option value="">Month</option>
                        <?php
                        for($i=1; $i<=12; $i++){
                            if($i==$date[1]){
                                $key = 'selected';
                            }else{
                                $key = '';
                            }
                            echo "<option $key value='$i'>$i</option>";
                        }
                        ?>
                    </select>
                    <select name="year" class="btn btn-default">
                        <option value="">Year</option>
                        <?php
                        for($i=1990; $i<=2017; $i++){
                            if($i==$date[2]){
                                $key = 'selected';
                            }else{
                                $key = '';
                            }
                            echo "<option $key value='$i'>$i</option>";
                        }
                        ?>
                    </select>
                </div>
                <div class="form-group">
                    <button type="submit" class="btn btn-primary">Submit</button>
                    <button type="reset" class="btn btn-primary">Reset</button>
                </div>

            </form>
        </div>
    </div>

</div>
</body>
</html>
