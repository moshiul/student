<?php

$db = new PDO('mysql:host=localhost;dbname=Student;charset=utf8mb4', 'root', '');
$query="SELECT * FROM `std-reg`where id=".$_GET['id'];
$stmt=$db->query($query);
$data=$stmt->fetch(PDO::FETCH_ASSOC);

?>




<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title></title>
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/bootstrap-theme.css">


    <style>
        label{
            display: block;
            border-bottom: 2px solid #ccc;
            padding: 7px 5px;

        }


    </style>
</head>

<body>
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <nav class="navbar navbar-default">
                <div class="container-fluid">
                    <div class="navbar-header">
                        <a class="navbar-brand" href="#">Student Info</a>
                    </div>
                    <ul class="nav navbar-nav">

                        <li><a href="reg.php">Registration</a></li>
                        <li><a href="viewStudent.php">All Student</a></li>
                        <li><a href="course.php">Course</a></li>

                        <li><a href="assign.php">Assign</a></li>
                    </ul>
                </div>
            </nav>


        </div>
    </div>
</div>


<div class="container ">
    <div class="col-md-6">
        <h3 align="center">Your Details Information</h3>

        <table class="table table-bordered">
            <tr>
                <td align="center">
                    <b>Your Name:
                </td>
                <td>
                    <?php echo $data['name']?>
                </td>
            </tr>

            <tr>
                <td align="center">
                    <b>Your Email:
                </td>
                <td>
                    <?php echo $data['email']?>
                </td>
            </tr>

            <tr>
                <td align="center">
                    <b>Your Password:
                </td>
                <td>
                    <?php echo $data['password']?>
                </td>
            </tr>

            <tr>
                <td align="center">
                    <b>Your Address:
                </td>
                <td>
                    <?php echo $data['address']?>
                </td>
            </tr>

            <tr>
                <td align="center">
                    <b>Your Contract No:
                </td>
                <td>
                    <?php echo $data['mobile']?>
                </td>
            </tr>

            <tr>
                <td align="center">
                    <b>Your Gender:
                </td>
                <td>
                    <?php echo $data['gender']?>
                </td>
            </tr>

            <tr>
                <td align="center">
                    <b>Your Profile Picture:
                </td>
                <td>
                    <img src="images/<?php echo $data['picture']?>" alt="Smiley face" height="142" width="142">
                </td>
            </tr>

            <tr>
                <td align="center">
                    <b>Date of Birth:
                </td>
                <td>
                    <?php echo $data['dob']?>
                </td>
            </tr>
        </table>
    </div>

</div>





</body>
</html>
