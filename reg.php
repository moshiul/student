<html>
<head>
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/bootstrap-theme.css">

</head>
<body>


<div class="container">
    <div class="row">
        <div class="col-md-12">
            <nav class="navbar navbar-default">
                <div class="container-fluid">
                    <div class="navbar-header">
                        <a class="navbar-brand" href="#">Student Info</a>
                    </div>
                    <ul class="nav navbar-nav">

                        <li><a href="reg.php">Registration</a></li>
                        <li><a href="viewStudent.php">All Student</a></li>
                        <li><a href="course.php">Course</a></li>

                        <li><a href="assign.php">Assign</a></li>
                    </ul>
                </div>
            </nav>


        </div>
    </div>
</div>

<div class="container">
    <div class="row">
        <div class="col-md-6">
            <form action="insertStudent.php" method="post" enctype="multipart/form-data">

                <div class="box">

                    <div class="form-group">
                        <label> Enter Your Name </label>
                        <input type="text" name="name" class="form-control">
                    </div>

                    <div class="form-group">
                        <label> Enter Your Email </label>
                        <input type="text" name="email" class="form-control">
                    </div>

                    <div class="form-group">
                        <label> Enter Your Password </label>
                        <input type="text" name="password" class="form-control">
                    </div>

                    <div class="form-group">
                        <label> Enter Your Address </label>
                        <textarea name="address" name="address" class="form-control"></textarea>
                    </div>

                    <div class="form-group">
                        <label> Enter Your Mobile </label>
                        <input type="text" name="mobile" class="form-control">
                    </div>
                </div>

                <div class="form-group">
                    <label>Select Your Gender</label>

                    <input  id="male" type="radio" name="gender" value="male">
                    <label for="male">Male</label>

                    <input type="radio" id="female" name="gender" value="female">
                    <label for="female">Female</label>
                </div>



                <div class="form-group">
                    <label>Choose Your Profile Picture</label>
                    <input type="file" id="file" class="custom-file-input" name="image">
                </div>


                    <div class="form-group">
                        <label>Select your DOB:</label>
                        <select name="day" class="btn btn-default">
                            <option value="">Day</option>
                            <?php
                            for($i=1; $i<=31; $i++){

                                echo "<option  value='$i'>$i</option>";
                            }
                            ?>


                        </select>
                        <select name="month" class="btn btn-default">
                            <option value="">Month</option>
                            <?php
                            for($i=1; $i<=12; $i++){

                                echo "<option  value='$i'>$i</option>";
                            }
                            ?>
                        </select>
                        <select name="year" class="btn btn-default">
                            <option value="">Year</option>
                            <?php
                            for($i=1990; $i<=2017; $i++){

                                echo "<option  value='$i'>$i</option>";
                            }
                            ?>
                        </select>
                    </div>
                <div class="form-group">
                    <button type="submit" class="btn btn-primary">Submit</button>
                    <button type="reset" class="btn btn-primary">Reset</button>
                </div>

            </form>
        </div>
    </div>

</div>
</body>
</html>