<?php
$db = new PDO('mysql:host=localhost;dbname=Student;charset=utf8mb4', 'root', '');

$query="SELECT * FROM `std-reg` ORDER BY id ASC ";
$stmt=$db->query($query);
$studentName=$stmt->fetchAll(PDO::FETCH_ASSOC);

$query="SELECT * FROM `course` ORDER by course_title";
$stmt=$db->query($query);
$courseTitle=$stmt->fetchAll(PDO::FETCH_ASSOC);
//var_dump($studentName);
?>


<html>
<head>
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/bootstrap-theme.css">
</head>

<body>

<div class="container">
    <div class="row">
        <div class="col-md-12">
            <nav class="navbar navbar-default">
                <div class="container-fluid">
                    <div class="navbar-header">
                        <a class="navbar-brand" href="#">Student Info</a>
                    </div>
                    <ul class="nav navbar-nav">

                        <li><a href="reg.php">Registration</a></li>
                        <li><a href="viewStudent.php">All Student</a></li>
                        <li><a href="course.php">Course</a></li>

                        <li><a href="assign.php">Assign</a></li>
                    </ul>
                </div>
            </nav>


        </div>
    </div>
</div>


<div class="container">
    <div class="row">
        <div class=" col-md-offset-3 col-md-6">
            <form  action="insertCourseAssign.php" method="post">
                <div class="form-group">
                    <label for="course_code">Select Student :</label>
                    <select name="sname"  class="form-control">
                        <option value="" >Choose Student</option>
                        <?php
                            foreach ($studentName as $student){?>
                                <option value="<?php echo $student['id']?>"> <?php echo $student['id']?> </option>
                            <?php
                                }
                            ?>

                    </select>
                </div>
                <div class="form-group">
                    <label for="course_title">Select Course</label>
                    <?php
                    foreach ($courseTitle as $check){
                        ?>
                        <input type="checkbox" name="check[]" value="<?php echo $check['course_title']?>"><?php echo $check['course_title'];?>
                        <?php
                    }
                    ?>
                </div>

                <button type="submit" class="btn btn-success">Submit</button>
            </form>
        </div>
    </div>





</body>
</html>
